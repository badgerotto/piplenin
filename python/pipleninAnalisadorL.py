import pipleninTokens
import os.path
import string

class AnalisadorArquivos():
    arquivo_entrada = " "

    def setArquivoEntrada(self, arquivo):
        self.arquivo_entrada = arquivo

    def analiseLexica(self):
        nlin = 0
        t = pipleninTokens.Tokens().getTokens()
        for ll in self.arquivo_entrada:
            l = self.arquivo_entrada.readline()
            v = l.split( )
            nlin +=1
            tokens = list(set(t) & set(v))
            for i in tokens:
                print ("Token encontrado: %s , na linha: %d" % (i, nlin) )
        else:
            self.arquivo_entrada.close()
            print("Operacao Finalizada")
                