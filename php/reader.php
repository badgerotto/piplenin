<?php

$tokens = token_get_all('');

foreach ($tokens as $token) {
    if (is_array($token)) {
        echo "Linha {$token[2]}: ", token_name($token[0]), " ('{$token[1]}')", PHP_EOL;
    }
}
?>