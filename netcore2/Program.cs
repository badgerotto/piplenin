﻿using System;
using System.IO;

namespace netcore2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite o nome do arquivo: ");
            string arquivo = Console.ReadLine();
            TextReader Leitor = new StreamReader(arquivo, true);
            int nlin = 0;
            while (Leitor.Peek() != -1) 
            {
                nlin++;
                Leitor.ReadLine();
            }
            Leitor.Close(); 
            Console.WriteLine("Este Arquivo contém " + nlin + " Linhas.");
            Console.ReadKey();
        }
    }
}
