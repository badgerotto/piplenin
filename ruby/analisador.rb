require_relative 'analise_lexica'
# Define função main
def main

    # Inicia leirura do arquivo para análise lexica
    aL = AnaliseLexica.new 'arquivo.birl'
    # Inicia a análise
    aL.start
end

# Chamada para a função
main