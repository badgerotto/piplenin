require_relative 'token'
class AnaliseLexica
    @qtdLines = 0
    @validLine = false
    @file
    def initialize(file)
        # Abre o arquivo em modo leitura
        @file = File.new file, "r"
    end

    def start
        # Itera entre as linhas do arquivo
        i = 0
        # Inicia classe de tokens
        token = Token.new

        # Itera entre as linhas do arquivo
        while line = @file.gets
            line.split(" ").each do |text|#ler cada palavra e colocar em uma variavel 
                if  !token.get.include? text
                    puts "Token inválido " + text + ". Na linha: #{i}"
                end
            end
            i += 1
        end
        @qtdLines = i
        @file.close
        # Exibe o total de linhas
        @qtdLines
    end
end