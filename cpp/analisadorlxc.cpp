#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

using namespace std;

// Inicia o main com retorno em int
int main () {
	// Defininindo vari�veis necess�rias para a contagem de linhas do arquivo - Number of Lines e Line -
	int noflines = 0;
    string line;
    char * stsplit;
    char * token[14];
    char * jumper;
    int i = 0;
    
	// Define vari�vel de abertura e somente leitura de arquivo
	ifstream fop;
	
	// Abre arquivo .txt para leitura -- arquivo de teste --
	fop.open ("arquivo.txt");
		
		// Contador linhas com o While -- Tirar o EOF? --
		while(!fop.eof()) {
			// Identifica e 'pega' a linha
			getline(fop, line);
			//in�cio do c�digo novo
			jumper = strdup(line.c_str());
			stsplit = strtok(jumper, " ");
			token[i] = stsplit;
			
			while (stsplit != NULL)
			{
				token[i] = stsplit;
				stsplit = strtok(jumper, " ");
				i ++;
				free(jumper);
			}
			// Para cada linha que ele identificar, acrescenta +1 na vari�vel do contador Number of Lines
			noflines ++;
		}
		
		// Mensagem com n�mero total de linhas no arquivo
		cout << "N�mero de linhas no arquivo: " << noflines;
		
	// Fechando arquivo
	fop.close();
	
			
	
	// Retornando para o main
	return 0;
	
	//Pausando o sistema
	system("PAUSE");
}
