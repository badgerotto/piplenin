 Vladimir Ilyich Ulyanov, 
 mais conhecido pelo pseudônimo Lenin, 
 foi um revolucionário comunista, 
 político e teórico político russo 
 que serviu como chefe de governo da República Russa de 1917 a 1918, 
 da República Socialista Federativa Soviética da Rússia de 1918 a 1922 
 e da União Soviética de 1922 a 1924. 

 Sob sua administração, 
 a Rússia e, 
 em seguida, 
 a União Soviética 
 tornaram-se um Estado socialista unipartidário governado pelo Partido Comunista. 
 Ideologicamente marxistas, suas teorias políticas são conhecidas como leninismo.