package org.jorge.base;

import java.io.File;
import java.nio.file.Paths;

public class Compile {

	public static void main(String[] args) {
		
		AnaliseLexica analiseLexica = new AnaliseLexica();
		
		String path = new File("").getAbsolutePath();
		String file = Paths.get(path, "TESTE.java").toString();
		
		analiseLexica.AnalisarArquivo(file);
	}

}
