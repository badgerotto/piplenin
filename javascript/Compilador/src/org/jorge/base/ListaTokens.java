package org.jorge.base;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ListaTokens {
	private static ArrayList<String> tokens;

	private static ArrayList<String> getTokens() {
		if (tokens == null) {
			 ListaTokens.tokens = createTokensArrayList();
		}
		return tokens;
	};

	public static boolean isToken(String value) {
		if (tokens == null) {
			 ListaTokens.tokens = createTokensArrayList();
		}

		if (value.trim().isEmpty()) {
			return true;
		}

		return tokens.contains(value.trim().toUpperCase());
	}
	
	private static ArrayList<String> createTokensArrayList() {
		ArrayList<String> tokens = new ArrayList<String>();
		tokens.add("PUBLIC");
		tokens.add("PRIVATE");
		tokens.add("CLASS");
		tokens.add("STATIC");
		tokens.add("IF");
		tokens.add("ELSE");
		tokens.add("{");
		tokens.add("}");
		tokens.add("VOID");
		tokens.add("(");
		tokens.add(")");
		tokens.add("[");
		tokens.add("]");
		tokens.add("ARGS");
		tokens.add(";");
		
		
		return tokens;
	}
}
