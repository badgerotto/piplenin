package org.jorge.base;

import java.io.BufferedReader;
import java.io.FileReader;

public class AnaliseLexica {

	public void AnalisarArquivo(String nomeArquivo) {
		int countLine = 0;
		boolean validLine = false;

		AnaliseSintatica sintaxe = new AnaliseSintatica();

		try {
			FileReader arquivo = new FileReader(nomeArquivo);
			BufferedReader lerArquivo = new BufferedReader(arquivo);

			String line = null;
			String[] vLine;
			String messageLine = null;

			while ((line = lerArquivo.readLine()) != null) {

				countLine++;
				validLine = false;
				messageLine = null;

				if (!line.trim().isEmpty() && !IsCommentedLine(line)) {
					vLine  =line.split(" ");
					
					for(String value: vLine){
						
						if (!ListaTokens.isToken(value)){
							messageLine += ("Linha: " + countLine + "Token inv�lido: " + value + "\n");
						}
					}
					
					if (validLine){
						messageLine = "";
					} else{
						System.out.println(messageLine);
					}
 
				}
			}
			arquivo.close();

			System.out.println("An�lise Realizada com Sucesso");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Um erro ocorreu " + e.getLocalizedMessage());
		}
	}

	private boolean IsCommentedLine(String line) {
		if (line != null) {
			return line.startsWith("//");
		}
		return false;
	}

}
